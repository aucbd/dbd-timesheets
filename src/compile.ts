import {PDFDocument, PDFForm, PDFPage, PDFTextField, rgb} from "pdf-lib"
import fs = require("fs");

export interface ITimesheet {
    name: string;
    supervisor: string;
    project: string;
    study: "bde" | "bsc" | "merc" | "polyt" | "gmm" | "ha" | "int" | "none";
    hours: [string, string, number][];
    signDate: string;
    signature: string;
}

export async function compileTimesheet(timesheetData: ITimesheet, templatePath: string): Promise<Buffer> {
    const timesheet: PDFDocument = await PDFDocument.load(fs.readFileSync(templatePath))
    const page: PDFPage = timesheet.getPage(0)
    const form: PDFForm = timesheet.getForm()
    const height: number = page.getSize().height

    // Name
    page.drawText(timesheetData.name, {x: 94, y: height - 185, size: 11})

    // CPR
    const cprField: PDFTextField = form.createTextField("info.cpr")
    cprField.setText("")
    cprField.addToPage(page, {x: 390, y: height - 188.5, height: 15, width: 148})

    // Supervisor
    page.drawText(timesheetData.supervisor, {x: 288, y: height - 208.5, size: 11})

    // Study
    if (timesheetData.study === "bde") page.drawText("X", {x: 62.3, y: height - 255.3, size: 14})
    else if (timesheetData.study === "bsc") page.drawText("X", {x: 62.3 + 120.6, y: height - 255.3, size: 14})
    else if (timesheetData.study === "merc") page.drawText("X", {x: 62.3 + (120.6 * 2), y: height - 255.3, size: 14})
    else if (timesheetData.study === "polyt") page.drawText("X", {x: 62.3 + (120.6 * 3), y: height - 255.3, size: 14})
    else if (timesheetData.study === "gmm") page.drawText("X", {x: 62.3, y: height - 277.8, size: 14})
    else if (timesheetData.study === "ha") page.drawText("X", {x: 62.3 + 120.6, y: height - 277.8, size: 14})
    else if (timesheetData.study === "int") page.drawText("X", {x: 62.3 + (120.6 * 2), y: height - 277.8, size: 14})
    else if (timesheetData.study === "none") page.drawText("X", {x: 62.3 + (120.6 * 3), y: height - 277.8, size: 14})

    // Project
    page.drawRectangle({
        x: 56.6,
        y: height - 299.5,
        height: 15,
        width: 482,
        opacity: 0,
        borderColor: rgb(0, 0, 0),
        borderWidth: 0.5
    })
    page.drawText(`Project: ${timesheetData.project}`, {x: 62, y: height - 295.5, size: 11})

    // Hours
    timesheetData.hours.forEach(([date, work, hours], i) => {
        page.drawText(date, {x: 62.3, y: height - (355 + (23.4 * i)), size: 11})
        page.drawText(work, {x: 129, y: height - (355 + (23.4 * i)), size: 11})
        page.drawText(String(hours), {x: 470, y: height - (355 + (23.4 * i)), size: 11})
    })

    // Sign date
    page.drawText(timesheetData.signDate, {x: 62.3, y: height - 613, size: 11})

    return Buffer.from(await timesheet.save())
}